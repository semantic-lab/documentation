import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import mdx from '@mdx-js/rollup';
import rehypeHighlight from 'rehype-highlight';
import remarkGfm from 'remark-gfm';
import remarkFrontmatter from 'remark-frontmatter'
import remarkMdxFrontmatter from 'remark-mdx-frontmatter'

const mdxOptions = {
    rehypePlugins: [ rehypeHighlight ],
    remarkPlugins: [ remarkGfm, remarkFrontmatter, remarkMdxFrontmatter ],
};

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [ react(), mdx(mdxOptions) ],
});
