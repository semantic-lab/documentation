import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import '../assets/components/article.scss';
import RoutingMiddleware from './RoutingMiddleware.tsx';
import RoutesConfig from '../configs/routes.tsx';
import NotFound from './NotFound.tsx';
import Home from './Home.tsx';

const routes = [
    {
        path: '/*',
        element: <RoutingMiddleware />,
        children: [
            {
                path: '',
                element: <Home />
            },
            ...RoutesConfig,
            {
                path: '*',
                element: <NotFound />,
            }
        ],
    }
];

export const router = createBrowserRouter(routes);

export default function article() {
    return (
        <div className="article">
            <RouterProvider router={router}/>
        </div>
    );
}

