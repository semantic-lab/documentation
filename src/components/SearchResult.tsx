import '../assets/components/search-result.scss';
import { computed } from '@preact/signals-react';
import LayoutState from '../states/layout.ts';
import SearchState from '../states/search.ts';
import SideMenuServices, { TreeItem } from '../core/side-menu-services.ts';
import RouteState from '../states/route.ts';
import { CiFolderOn } from "react-icons/ci";
import { SiMdx } from "react-icons/si";

export default function SearchResult () {
    const cardComponents = computed(filterSideMenuList);

    function filterSideMenuList() {
        return SideMenuServices
            .filterBySearchCriteria(LayoutState.sideMenuList.value, SearchState.searchCriteria.value)
            .map((data: TreeItem) => <ResultCard data={data} key={data.href} />);
    }


    return (
        <div className="search-result">
            <div className="container">{ cardComponents }</div>
        </div>
    );
}

function ResultCard({ data }: { data: TreeItem }) {

    function onClickItem() {
        SideMenuServices.clickOnSideMenuComponent([
            data,
            LayoutState.sideMenuList.value,
            LayoutState.openedDirs.value,
            LayoutState.setOpenedDirs,
            RouteState.setNavigateTarget,
        ]);
        LayoutState.setOpenedDirs(SideMenuServices.getOpenDirsByHref(LayoutState.sideMenuList.value, data.href, LayoutState.openedDirs.value));
        LayoutState.setPopupType('');
        SearchState.setSearchCriteria(undefined);
    }

    return (
        <div className="result-card" onClick={onClickItem}>
            <div className="name">{data.name}</div>
            <hr />
            <div className="path">{data.href}</div>
            <div className="icon">
                { data.isDir && <CiFolderOn /> }
                { !data.isDir && <SiMdx /> }
            </div>
        </div>
    );
}
