import SearchState from '../states/search.ts';
import LayoutState from '../states/layout.ts';
import SearchServices from '../core/search-services.ts';
import { useLifeHooks } from '../expansions/hook.ts';

export default function Home() {
    // region LIFE-HOOKS
    useLifeHooks(openSearchPopup);
    // endregion

    // region METHODS
    function openSearchPopup() {
        SearchServices.search([
            { '*': '' },
            SearchState.setSearchCriteria,
            LayoutState.setPopupType,
        ]);
    }
    // endregion

    return (
        <div className="home"></div>
    );
}
