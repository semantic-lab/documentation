import '../assets/components/nav.scss';
import { MdDehaze } from 'react-icons/md';
import LayoutState from '../states/layout.ts';
import SearchBar from './SearchBar.tsx';
import LayoutConfig from '../configs/layout.tsx';
import XConditions from '../utils/x-conditions.ts';

type LogoType = { text: string|null; image: string|null; element: JSX.Element|null; };

export default function Nav() {

    function toggleSideMenu() {
        LayoutState.setSideMenuCollapsed(!LayoutState.sideMenuCollapsed.value);
    }

    return (
        <div className="nav">
            <div className="left">
                <div className="btn m border menu-icon" onClick={toggleSideMenu} ><MdDehaze /></div>
                <Logo />
            </div>
            <div className="center">
                <SearchBar />
            </div>
            <div className="right">
                <div className="img-container">
                    <a target="0" href="https://calendar.google.com/calendar/u/0/r">
                        <img alt="Google Calendar" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Google_Calendar_icon_%282020%29.svg/240px-Google_Calendar_icon_%282020%29.svg.png"/>
                    </a>
                </div>
                <div className="img-container">
                    <a target="1" href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox">
                        <img alt="Gmail" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Gmail_icon_%282020%29.svg/320px-Gmail_icon_%282020%29.svg.png"/>
                    </a>
                </div>
            </div>
        </div>
    );
}


function Logo() {
    // region DATA
    const { element, image, text } = LayoutConfig.logo as LogoType;
    const style: Record<string, string> = {};
    const type = getLogoType();
    // endregion

    // region METHODS
    function getLogoType() {
        return XConditions.switchingSync([
            {
                condition: () => !!element,
                exec: () => 0,
            },
            {
                condition: () => !!image,
                exec: () => {
                    style.backgroundImage = `url(${ image as string })`;
                    return 1;
                },
            },
            {
                condition: () => !!text,
                exec: () => 2,
            }
        ]);
    }
    // endregion

    return (
        <div className="logo" style={style}>
            {type === 0 && element}
            {type === 2 && <span>{text}</span>}
        </div>
    );
}
