import '../assets/components/not-found.scss';
import { IoMdPlanet } from "react-icons/io";

export default function NotFound() {
    return (
        <div className="not-found">
            <div className="message">
                <div>404</div>
                <div>Meow ... We are lost in Docs</div>
            </div>
            <div className="path">
                <div className="cat"></div>
            </div>
            <div className="background">
                <IoMdPlanet />
            </div>
        </div>
    );
}
