import '../assets/components/side-menu.scss';
import LayoutState from '../states/layout.ts';
import { computed, effect } from '@preact/signals-react';
import RouteState from '../states/route.ts';
import SideMenuServices, { TreeItem } from '../core/side-menu-services.ts';
import { MdArrowDropDown, MdArrowRight } from 'react-icons/md';
import { SiMdx } from "react-icons/si";

export default function SideMenu() {
    // region DATA
    const shownSideMenuList = computed(generateShownSideMenu);
    const sideMenuItemComponents = computed(generateSideMenuItemComponent);
    const containerComponent = computed(modifyContainerComponent);
    // endregion

    // region WATCH
    effect(initOpenDirs);
    // endregion

    // region METHODS
    function initOpenDirs() {
        if (!RouteState.isFirstLoad.value) return;
        LayoutState.openedDirs.value = SideMenuServices.getOpenDirsByHref(LayoutState.sideMenuList.value, RouteState.url.value);
    }

    function generateShownSideMenu() {
        return SideMenuServices.generateShownSideMenu(RouteState.url.value, LayoutState.sideMenuList.value, LayoutState.openedDirs.value);
    }

    function generateSideMenuItemComponent() {
        return shownSideMenuList.value.map(toSideMenuItemComponent);
    }

    function toSideMenuItemComponent(meta: TreeItem, index: number) {
        return <SideMenuItem meta={meta} key={index} />;
    }

    function modifyContainerComponent() {
        // region DATA
        const className = SideMenuServices.computedSideMenuContainerComponent(LayoutState.sideMenuCollapsed.value);
        // endregion

        return (
            <div className={className}>
                <div className="container">{ sideMenuItemComponents }</div>
            </div>
        );
    }
    // endregion

    return (
        <>{ containerComponent }</>
    );
}

function SideMenuItem({ meta }: { meta: TreeItem }) {
    // region DATA
    const [ className, isCollapsedDir, isOpenedDir, isFile ] = SideMenuServices.computedSideMenuItemComponentInfo(meta);
    // endregion

    // region METHODS
    function clickOnItem(meta: TreeItem) {
        SideMenuServices.clickOnSideMenuComponent([
            meta,
            LayoutState.sideMenuList.value,
            LayoutState.openedDirs.value,
            LayoutState.setOpenedDirs,
            RouteState.setNavigateTarget,
        ]);
    }
    // endregion

    return (
        <div className={className} onClick={() => clickOnItem(meta)}>
            <div className="icon" data-is-dir={meta.isDir}>
                {isCollapsedDir && <MdArrowRight/>}
                {isOpenedDir && <MdArrowDropDown />}
                {isFile && <SiMdx />}
            </div>
            <div>{ meta.name }</div>
        </div>
    );
}

