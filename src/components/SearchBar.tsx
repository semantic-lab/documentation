import '../assets/components/search-bar.scss';
import { LuSearch } from "react-icons/lu";
import { useSignal } from '@preact/signals-react';
import { FormEvent, KeyboardEvent } from 'react';
import SearchState from '../states/search.ts';
import LayoutState from '../states/layout.ts';
import SearchServices from '../core/search-services.ts';

export default function SearchBar() {
    // region DATA
    const keywords = useSignal<string>('');
    // endregion

    // region METHODS
    function syncKeyWords(event: FormEvent<HTMLInputElement>) {
        keywords.value = (event.nativeEvent.target as HTMLInputElement).value;
    }

    function searchByPressEnter(event: KeyboardEvent<HTMLInputElement>) {
        SearchServices.searchByPressEnter([
            event.nativeEvent,
            keywords.value,
            SearchState.setSearchCriteria,
            LayoutState.setPopupType,
        ]);
    }
    // endregion

    return (
        <div className="search-bar">
            <div className="icon"><LuSearch /></div>
            <input type="text" placeholder="search" onInput={syncKeyWords} onKeyUp={searchByPressEnter} />
        </div>
    );
}
