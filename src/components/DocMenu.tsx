import '../assets/components/doc-menu.scss';
import DocMenuServices, { DocMenuItemMeta } from '../core/doc-menu-services.ts';
import { computed, effect } from '@preact/signals-react';
import RouteState from '../states/route.ts';
import DocStore from '../states/doc.ts';

export default function DocMenu() {
    // region DATA
    const menuItemComponents = computed(generateMenuItemComponent);
    // endregion

    // region WATCH
    effect(navigateToParagraphWhenDocLoaded);
    // endregion

    // region METHODS
    function generateMenuItemComponent() {
        return DocStore.metaItems.value.map(toDocMenuItemComponent);
    }

    function toDocMenuItemComponent(meta: DocMenuItemMeta, index: number) {
        return <DocMenuItem meta={meta} index={index} key={index}/>;
    }

    async function navigateToParagraphWhenDocLoaded() {
        const tempMetas = DocMenuServices.getMenuItemMetas(RouteState.url.value);
        if (tempMetas.length === 0) {
            // using mutator might cause '@preact Error: Cycle detected'
            DocStore.metaItems.value = [];
            return;
        }

        const activeTargetIndex = DocMenuServices.findIndexByHash(RouteState.hash.value, tempMetas);
        await DocMenuServices.navigateToItem([
            tempMetas,
            activeTargetIndex,
            RouteState.setHash,
            DocStore.setIsScrollingManually,
            DocStore.setMetaItems,
        ]);
    }
    // endregion

    return (
        <div className="doc-menu">{menuItemComponents}</div>
    );
}

function DocMenuItem({ meta, index }: { meta: DocMenuItemMeta; index: number; }) {
    // region DATA
    const { text, classList } = meta;
    const className = classList.join(' ');
    // endregion

    // region METHODS
    async function navigate(index: number) {
        await DocMenuServices.navigateToItem([
            DocStore.metaItems.value,
            index,
            RouteState.setHash,
            DocStore.setIsScrollingManually,
            DocStore.setMetaItems,
        ]);
    }
    // endregion

    return (
        <div className={className} onClick={() => navigate(index)}>{text}</div>
    );
}


