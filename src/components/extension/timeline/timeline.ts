export type TimeLineArgs = {
    items: Array<TimelineItem>;
    unit?: Unit;
    range?: { from: string; to: string; };
}

export type Unit = 'date' | 'week' | 'month';

export type TimelineItem = {
    from: string;
    to?: string;
    title: string;
    link?: string;
    color?: string;
};

export type DateRange = [ current: number, next: number ];

export default {
    getBoundaries(timelineItems: Array<TimelineItem>, range?: { from: string; to: string; }) {
        if (range) return [ range.from, range.to ];

        const copied = [ ...timelineItems ];
        const startDateTime = copied.sort(ascByFrom)[0];
        const endDateTime = copied.sort(descByTo)[0];
        return [
            startDateTime.from,
            (endDateTime.to || endDateTime.from),
        ];
    },
    calculateDateList,
    calculateUnitList(dateList: Array<DateRange>, unit: Unit) {
        if (unit === 'month') return calculateMonthList(dateList);
        if (unit === 'week') return calculateWeekList(dateList);
        return dateList;
    },
    dateStr,
    getBarData(unitList: Array<[ current: number, next: number ]>, item: TimelineItem) {
        const from = fromTimestamp(item.from);
        const to = toTimestamp(item.from, item.to);
        const startIndex = unitList.findIndex(([ c, n ]) => c <= from && from < n);
        const endIndex = unitList.findIndex(([ c, n ]) => c <= to && to < n);

        const [ boundaryStartCurrent, boundaryStartNext ] = unitList[startIndex];
        const [ boundaryEndCurrent, boundaryEndNext ] = unitList[endIndex];
        const microStartMargin = (from - boundaryStartCurrent) / (boundaryStartNext - boundaryStartCurrent);
        const microEndMargin = (boundaryEndNext - to) / (boundaryEndNext - boundaryEndCurrent);

        return {
            marginLeft: `${ (startIndex + microStartMargin) / unitList.length * 100 }%`,
            width: `${ ((endIndex - microStartMargin) - (startIndex -  - microEndMargin) + 1) / unitList.length * 100 }%`
        };
    },
    navigate(link?: string) {
        if (!link) return;
        window.location.href = link;
    },
    nowTimelineItem(now: number) {
        const [ yyyy, MM, dd, , hh, mm, ss ] = dateStr(now);
        return {
            from: `${ yyyy }-${ MM }-${ dd } ${ hh }:${ mm }:${ ss }`
        } as TimelineItem;
    },
    updateNow(unit: Unit, frequency: number, nowMutator: (now: number) => void) {
        if ([ 'month', 'week' ].includes(unit)) return;
        return setInterval(() => nowMutator(Date.now()), frequency);
    },
    inUnit(from: number, target: number, to: number) {
        return from <= target && target <= to;
    },
    scrollToToday(rootElement: HTMLElement | null) {
        if (!rootElement) return;

        const container = rootElement.querySelector('.container');
        const today = rootElement.querySelector('.unit-item.today');
        if ([ container, today ].includes(null)) return;

        const index = [...rootElement.querySelectorAll('.unit-item')].indexOf(today as HTMLElement);
        const left = (today as HTMLElement).getBoundingClientRect().width * index;
        (container as HTMLElement).scrollTo({ left, behavior: 'smooth' });
    },
    WEEK_NAME: {
        W1: 'SUN',
        W2: 'MON',
        W3: 'TUE',
        W4: 'WED',
        W5: 'THU',
        W6: 'FRI',
        W7: 'SAT',
    } as Record<string, string>,
    MONTH_NAME: {
        '01': 'January',
        '02': 'February',
        '03': 'March',
        '04': 'April',
        '05': 'May',
        '06': 'June',

        '07': 'July',
        '08': 'August',
        '09': 'September',
        '10': 'October',
        '11': 'November',
        '12': 'December',
    } as Record<string, string>,
};

const DAY = 1000 * 60 * 60 * 24;

function midnightTimestampETL(dateTime: string) {
    const date = /^\d{4}-\d{2}-\d{2}/g.exec(dateTime);
    const dtStr = `${ date } 00:00:00`;
    return (new Date(dtStr)).getTime();
}

function fromTimestamp(dateTime: string) {
    const onlyDate = /^\d{4}-\d{2}-\d{2}$/g.test(dateTime);
    const dtStr = onlyDate ? `${ dateTime } 00:00:00` : dateTime;
    return (new Date(dtStr)).getTime();
}

function toTimestamp(fromDateTime: string, toDateTime?: string) {
    if (toDateTime) {
        const onlyDate = /^\d{4}-\d{2}-\d{2}$/g.test(toDateTime);
        const dtStr = onlyDate ? `${ toDateTime } 23:59:59` : toDateTime;
        return (new Date(dtStr)).getTime();
    }

    const fromDate = /^\d{4}-\d{2}-\d{2}/g.exec(fromDateTime);
    const dtStr = `${ fromDate } 23:59:59`;
    return (new Date(dtStr)).getTime();
}

function ascByFrom(a: TimelineItem, b: TimelineItem) {
    return midnightTimestampETL(a.from) - midnightTimestampETL(b.from);
}

function descByTo(a: TimelineItem, b: TimelineItem) {
    return midnightTimestampETL(b.to || b.from) - midnightTimestampETL(a.to || a.from);
}

function getRangeDetail(from: string, to: string, unit = 1) {
    const startBoundary = midnightTimestampETL(from);
    const endBoundary = midnightTimestampETL(to) + DAY;
    const totalUnitCount = (endBoundary - startBoundary) / (DAY * unit);

    return [ startBoundary, endBoundary, totalUnitCount ];
}

function calculateDateList(from: string, to: string): Array<[current: number, next: number]> {
    const [startBoundary, , totalDateCount]  = getRangeDetail(from, to);
    return new Array(totalDateCount)
        .fill(undefined)
        .map((_, index: number) => {
            const currentDate = startBoundary + (DAY * index);
            const nextDate = startBoundary + (DAY * (index + 1));
            return [ currentDate, nextDate ];
        });
}

function calculateWeekList(dateList: Array<[current: number, next: number]>) {
    const weekList: Array<[current: number, next: number]> = [];
    let week: Array<number> = [];
    dateList.forEach(([ current, next ], index: number) => {
        const currentWeekday = (new Date(current)).getDay();

        if (index === 0 || currentWeekday === 0) {
            week[0] = current;
        }

        if (currentWeekday === 6 || index === (dateList.length - 1)) {
            week[1] = next;
            weekList.push(week as [number, number]);
            week = [];
        }
    });
    return weekList;
}

function calculateMonthList(dateList: Array<[current: number, next: number]>) {
    const monthList: Array<[current: number, next: number]> = [];
    let month: Array<number> = [];
    let latestMonth: number|undefined = undefined;
    dateList.forEach(([ current, next ], index: number) => {
        const currentMonth = (new Date(current)).getMonth();
        const nextMonth = (new Date(next)).getMonth();

        if (currentMonth !== latestMonth) {
            month[0] = current;
            latestMonth = currentMonth;
        }

        if (nextMonth !== latestMonth || index === (dateList.length - 1)) {
            month[1] = next;
            monthList.push(month as [number, number]);
            month = [];
        }
    });
    return monthList;
}

function dateStr(timestamp: number) {
    const dateInstance = new Date(timestamp);
    const year = dateInstance.getFullYear().toString();
    const month = (dateInstance.getMonth() + 1).toString().padStart(2, '0');
    const date = dateInstance.getDate().toString().padStart(2, '0');
    const weekDay = (dateInstance.getDay() || 7).toString().padStart(2, 'W');
    const hour = dateInstance.getHours().toString().padStart(2, '0');
    const minute = dateInstance.getMinutes().toString().padStart(2, '0');
    const second = dateInstance.getSeconds().toString().padStart(2, '0');

    return [ year, month, date, weekDay, hour, minute, second ];
}
