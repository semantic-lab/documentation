import './timeline.scss';
import TimelineServices, {DateRange, TimeLineArgs, TimelineItem, Unit} from './timeline.ts';
import { computed, useSignal } from '@preact/signals-react';
import { SlOptions } from 'react-icons/sl';
import { MouseEvent, useRef } from 'react';

export default function Timeline({ items, unit, range }: TimeLineArgs) {
    // region DATA
    const [ from, to ] = TimelineServices.getBoundaries(items, range);
    const timelineUnit = useSignal(unit || 'date');
    const dateList = TimelineServices.calculateDateList(from, to);
    const unitList = computed(generateUnitList);
    const dateItemComponents = computed(toDateItemComponents);
    const timeLineItemComponents = computed(toTimelineItemComponents);
    const optionBoxCollapsed = useSignal(true);
    const OptionBoxComponent = computed(generateOptionBoxComponent);
    const nowLineComponent = computed(generateNowLineComponent);
    const rootElement = useRef(null);
    // endregion

    // region METHODS
    function generateUnitList() {
        return TimelineServices.calculateUnitList(dateList, timelineUnit.value);
    }

    function toDateItemComponents() {
        return unitList.value.map(([ from, to ]: DateRange) => <UnitItem from={from} to={to} unit={timelineUnit.value} key={from} />);
    }

    function toTimelineItemComponents() {
        return items.map((item: TimelineItem, index: number) => <TimeLineItem unitList={unitList.value} item={item} key={`${item.from}-${ index }`} />);
    }

    function generateOptionBoxComponent() {
        return optionBoxCollapsed.value ? <></> : <OptionBox optionChange={modifyTimelineUnit} scrollToToday={scrollToToday} />;
    }

    function generateNowLineComponent() {
        return <NowLine unitList={unitList.value} unit={timelineUnit.value} />;
    }

    function toggleOptionBox() {
        optionBoxCollapsed.value = !optionBoxCollapsed.value;
    }

    function collapsedOptionBox() {
        optionBoxCollapsed.value = true;
    }

    function modifyTimelineUnit(unit: Unit) {
        toggleOptionBox();
        timelineUnit.value = unit;
    }

    function scrollToToday() {
        toggleOptionBox();
        TimelineServices.scrollToToday(rootElement.current);
    }
    // endregion

    return (
        <div className="timeline" ref={rootElement}>
            {
                // eslint-disable-next-line
                // @ts-ignore
                <div className="option-box" tabIndex="0" onBlur={collapsedOptionBox}>
                    <div className="option-icon" onClick={toggleOptionBox}><SlOptions/></div>
                    {OptionBoxComponent}
                </div>
            }
            <div className="container">
                <div>
                    <div className="date-items-container">{dateItemComponents}</div>
                    <div className="timeline-items-container">
                        { nowLineComponent }
                        { timeLineItemComponents }
                    </div>
                </div>
            </div>
        </div>
    );
}

function OptionBox({ optionChange, scrollToToday }: { optionChange: (unit: Unit) => void; scrollToToday: () => void }) {
    return (
        <div className="option-list">
            <div onClick={() => optionChange('date')}>Date</div>
            <div onClick={() => optionChange('week')}>Week</div>
            <div onClick={() => optionChange('month')}>Month</div>
            <hr />
            <div onClick={() => scrollToToday()}>Today</div>
        </div>
    );
}

function UnitItem({ from, to, unit }: { from: number; to: number; unit: Unit }) {
    // region DATA
    const unitStart = from;
    const unitEnd = to - 1;
    const [ , fromMonth, fromDate, fromWeekDay ] = TimelineServices.dateStr(unitStart);
    const [ , toMonth, toDate ] = TimelineServices.dateStr(unitEnd);
    const weekDayClassName = fromWeekDay.replace('W', 'w-');
    const todayClassName = TimelineServices.inUnit(unitStart, Date.now(), unitEnd) ? 'today' : '';
    const isShowDateRangeOfMonth = useSignal(false);
    const monthContentComponent = computed(toMonthContentComponent);
    // endregion

    // region METHODS
    function toMonthContentComponent() {
        return (
            <>
                {isShowDateRangeOfMonth.value && <div>{fromMonth}-{fromDate}</div>}
                <div>{TimelineServices.MONTH_NAME[fromMonth]}</div>
                {isShowDateRangeOfMonth.value && <div>{toMonth}-{toDate}</div>}
            </>
        );
    }

    function showDateRangeOfMonth() {
        isShowDateRangeOfMonth.value = true;
    }
    function hideDateRangeOfMonth() {
        isShowDateRangeOfMonth.value = false;
    }
    // endregion


    if (unit === 'month') {
        return (
            <div className={`unit-item month ${ todayClassName }`} onMouseOver={showDateRangeOfMonth} onMouseLeave={hideDateRangeOfMonth}>
                { monthContentComponent }
            </div>
        );
    }

    if (unit === 'week') {
        return (
            <div className={`unit-item week ${ todayClassName }`}>
                <div>{ fromMonth }-{ fromDate }  -  { toMonth }-{ toDate }</div>
            </div>
        );
    }

    return (
        <div className={`unit-item date ${weekDayClassName} ${todayClassName}`}>
            <div className="week-day">{TimelineServices.WEEK_NAME[fromWeekDay]}</div>
            <div className="date">{ fromMonth }-{ fromDate }</div>
        </div>
    );
}

function TimeLineItem({ unitList, item }: { unitList: Array<[number, number]>, item: TimelineItem; }) {
    // region DATA
    const inlineStyle = TimelineServices.getBarData(unitList, item);
    const barInlineStyle = { backgroundColor: item.color };
    const position = useSignal<{ top: number; left: number; }|undefined>(undefined);
    const timelineInfoComponent = computed(generateTimelineInfoComponent);
    // endregion

    // region METHODS
    function generateTimelineInfoComponent() {
        return position.value
            ? <TimelineInfo item={item} top={position.value.top} left={position.value.left} />
            : <></>;
    }

    function navigate() {
        TimelineServices.navigate(item.link);
    }

    function showTimelineInfo(e: MouseEvent<HTMLDivElement>) {
        position.value = {
            top: e.pageY - 75,
            left: e.pageX + 10,
        };
    }

    function collapseTimelineInfo() {
        position.value = undefined;
    }
    // endregion

    return (
        <div className="timeline-item" style={inlineStyle} onClick={navigate} onMouseOver={showTimelineInfo} onMouseLeave={collapseTimelineInfo}>
            <div className="bar" style={barInlineStyle}>
                <div>{ item.title }</div>
            </div>
            { timelineInfoComponent }
        </div>
    );
}

function TimelineInfo({ item, top, left }: { item: TimelineItem; top: number; left: number; }) {
    // region DATA
    const inlineStyle = {
        top,
        left,
        color: item.color,
        boxShadow: `0 0 4px 4px ${ item.color }55`,
    };
    // endregion

    return (
        <div className="timeline-info" style={inlineStyle}>
            <div className="time-range">
                <span>{ item.from }</span>
                { item.to && <span> - </span> }
                { item.to && <span>{ item.to }</span> }
            </div>
            <div className="title">{ item.title }</div>
        </div>
    );
}

function NowLine({ unitList, unit }: { unitList: Array<[number, number]>; unit: Unit }) {
    // region DATA
    const now = useSignal(Date.now());
    const inlineStyle = computed(calculateInlineStyle);
    const component = computed(reRender);
    const timer = useSignal<NodeJS.Timeout|undefined>(undefined);
    // endregion

    // region LIFE-HOOKS
    onRerender();
    // endregion

    // region METHODS
    function calculateInlineStyle() {
        const nowTimelineItem: TimelineItem = TimelineServices.nowTimelineItem(now.value);
        const { marginLeft } = TimelineServices.getBarData(unitList, nowTimelineItem);
        return { marginLeft };
    }

    function reRender() {
        return <div className="now-line" style={inlineStyle.value}></div>;
    }

    function nowMutator(value: number) {
        now.value = value;
    }

    function onRerender() {
        timer.value && clearInterval(timer.value);
        timer.value = TimelineServices.updateNow(unit,1000 * 60, nowMutator);
    }
    // endregion

    return (
        <>{ component }</>
    );
}
