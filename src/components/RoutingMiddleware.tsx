import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import { MouseEvent, useEffect } from 'react';
import RouteState from '../states/route.ts';
import LayoutState from '../states/layout.ts';
import SideMenuServices, { TreeItem } from '../core/side-menu-services.ts';
import routes from '../configs/routes.tsx';
import { useLifeHooks, useSignalEffects } from '../expansions/hook.ts';
import DocMenuServices from '../core/doc-menu-services.ts';
import XConditions from '../utils/x-conditions.ts';
import LayoutConfig from '../configs/layout.tsx';
import { computed, useSignal } from '@preact/signals-react';
import { IoIosInformationCircle } from 'react-icons/io';
import SearchServices from '../core/search-services.ts';
import SearchState from '../states/search.ts';
import { MdOutlineContentPasteSearch } from "react-icons/md";
import { WiStars } from "react-icons/wi";

export default function RoutingMiddleware() {
    // region DATA
    const location = useLocation();
    const navigate = useNavigate();
    const ArticleMetaComponent = computed(generateArticleMetaComponent);
    // endregion

    // region LIFE-HOOKS AND WATCH
    useLifeHooks(initSideMenuList);

    useEffect(() => {
        updateRoutingData();
        modifyDocumentTitle();
        // eslint-disable-next-line
    }, [ location ]);

    useSignalEffects([
        navigateToChapter,
        navigateToDocument,
    ]);
    // endregion

    // region METHODS
    function initSideMenuList() {
        LayoutState.setSideMenuList(SideMenuServices.toTreeList(routes));
    }

    function updateRoutingData() {
        RouteState.setURL(location.pathname);
        RouteState.setHash(location.hash);
        RouteState.setNavigateTarget(undefined);
    }

    function modifyDocumentTitle() {
        const target = targetOfHref(location.pathname);
        XConditions.switchingSync([
            {
                condition: () => !target,
                exec: () => document.title = LayoutConfig.projectName,
            },
            {
                condition: () => true,
                exec: () => document.title = (target as TreeItem).name,
            }
        ]);
    }

    function generateArticleMetaComponent() {
        const target = targetOfHref(location.pathname);
        return target ? <ArticleMeta target={target} /> : <></>;
    }

    function navigateToChapter() {
        DocMenuServices.browserNavigateToChapter([ RouteState.hash.value, location.hash ], navigate);
    }

    function navigateToDocument() {
        SideMenuServices.navigateToDocument(RouteState.navigateTarget.value, navigate);
    }

    function targetOfHref(href: string) {
        return SideMenuServices.findBySearchCriteria(LayoutState.sideMenuList.value, { href });
    }
    // endregion

    return (
        <>
            { ArticleMetaComponent }
            <Outlet />
        </>
    );
}


function ArticleMeta({ target }: { target: TreeItem }) {
    // region DATA
    const isPopped = useSignal(false);
    const MetaPopupComponent = computed(generateMetaPopupComponent);
    // endregion

    // region METHODS
    function generateMetaPopupComponent() {
        return isPopped.value ? <MetaPopup collapsedPopup={collapsedPopup} target={target} /> : <></>;
    }

    function togglePopup() {
        isPopped.value = !isPopped.value;
    }

    function collapsedPopup() {
        isPopped.value = false;
    }
    // endregion

    return (
        // eslint-disable-next-line
        // @ts-ignore
        <div className="meta" tabIndex="0" onClick={togglePopup} onBlur={collapsedPopup}>
            <div className="icon">
                <IoIosInformationCircle />
            </div>
            { MetaPopupComponent }
        </div>
    );
}

function MetaPopup({ collapsedPopup, target }: { collapsedPopup: () => void; target: TreeItem; }) {
    const { author, date, tags } = (target.meta || {});

    // region LIFE-HOOKS
    useLifeHooks(onMounted, onDestroyed);
    // endregion

    // region METHODS
    function freezeEvent(e: MouseEvent<HTMLDivElement>) {
        e.stopPropagation();
        e.preventDefault();
    }

    function onMounted() {
        window.addEventListener('keyup', pressEsc, true);
    }

    function onDestroyed() {
        window.removeEventListener('keyup', pressEsc, true);
    }

    function pressEsc(event: KeyboardEvent) {
        if (event.key !== 'Escape') return;
        collapsedPopup();
    }
    // endregion

    return XConditions.switchingSync([
        {
            condition: () => !target.meta,
            exec: () => (
                <div className="popup no-detail" onClick={freezeEvent}>
                    <div className="icon"><MdOutlineContentPasteSearch /></div>
                    <div className="icon"><WiStars/></div>
                    <div className="message">Oops! No Detail</div>
                </div>
            ),
        },
        {
            condition: () => !!target.meta,
            exec: () => (
                <div className="popup" onClick={freezeEvent}>
                    <CreateInfo author={author} date={date}/>
                    <Tags tags={tags} collapsedPopup={collapsedPopup}/>
                </div>
            ),
        }
    ]);
}

function CreateInfo({ author, date }: { author?: string; date?: string }) {
    const hasInfo = !!(author || date);
    return XConditions.switchingSync([
        {
            condition: () => !hasInfo,
            exec: () => <></>
        },
        {
            condition: () => hasInfo,
            exec: () => (
                <div className="name-date">
                    { author && <div>{author}</div> }
                    { author && date && <div>@</div> }
                    { date && <div>{date}</div> }
                </div>
            ),
        }
    ]) as JSX.Element;
}

function Tags({ tags: tagStrList, collapsedPopup }: { tags: string; collapsedPopup: () => void; }) {
    const tags = (tagStrList || '')
        .replace(/,\s*/g, ',')
        .split(',')
        .filter((tag: string) => !!tag);

    return XConditions.switchingSync([
        {
            condition: () => tags.length === 0,
            exec: () => <></>,
        },
        {
            condition: () => tags.length > 0,
            exec: () => {
                return (
                    <div className="tags container">
                        { tags.map((tag: string, index: number) => <Tag value={tag} key={index} collapsedPopup={collapsedPopup} />) }
                    </div>
                );
            },
        }
    ]);
}

function Tag({ value, collapsedPopup }: { value: string; collapsedPopup: () => void; }) {
    // region METHODS
    function searchByTag() {
        SearchServices.searchByClickOnTag([
            value,
            SearchState.setSearchCriteria,
            LayoutState.setPopupType,
        ]);
        collapsedPopup();
    }
    // endregion

    return (
        <div className="doc tag" onClick={searchByTag}>{ value }</div>
    );
}
