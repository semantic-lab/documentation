import '../assets/components/full-screen-popup.scss';
import { computed, effect, useSignal } from '@preact/signals-react';
import LayoutState from '../states/layout.ts';
import { dynamicLoadComponent } from '../expansions/component.tsx';
import { MouseEvent } from 'react';

export default function FullScreenPopup() {
    // region DATA
    const RootComponent = computed(calculateRootComponent);
    const component = useSignal<JSX.Element>(<></>);
    // endregion

    // region WATCH
    effect(resetComponent);
    effect(dynamicLoadSearchResultComponent);
    // endregion

    // region METHODS
    function calculateRootComponent() {
        const className = LayoutState.popupType.value
            ? 'full-screen-popup'
            : 'full-screen-popup collapsed';

        return (
            <div className={className} onClick={closePopup}>
                <div className="container" onClick={stopPropagation}>{component}</div>
            </div>
        );
    }

    function resetComponent() {
        if (LayoutState.popupType.value) return;
        component.value = <></>;
    }

    async function dynamicLoadSearchResultComponent() {
        if (!(LayoutState.popupType.value === 'search')) return;
        component.value = await dynamicLoadComponent('components/SearchResult.tsx');
    }

    function closePopup() {
        LayoutState.setPopupType('');
    }

    function stopPropagation(event:  MouseEvent<HTMLDivElement>) {
        event.stopPropagation();
        event.preventDefault();
        event.nativeEvent.stopPropagation();
        event.nativeEvent.preventDefault();
    }
    // endregion


    return (
        <>{ RootComponent }</>
    );
}
