import { Signal, signal } from '@preact/signals-react';

type Mutator<T> = (value: T) => void;

function createState<T>(defaultValue: T): [ Signal<T>, Mutator<T> ] {
    const state = signal<T>(defaultValue);
    function setter(value: T) {
        value !== state.value && (state.value = value);
    }

    return [ state, setter ];
}

function createNonState<T>(defaultValue: T): [ { value: T }, Mutator<T> ] {
    const state = { value: defaultValue };
    function setter(value: T) {
        value !== state.value && (state.value = value);
    }

    return [ state, setter ];
}

export default {
    createState,
    createNonState,
};
