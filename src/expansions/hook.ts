import { useEffect } from 'react';
import { effect } from '@preact/signals-react';

type SignalEffectHandler = () => void;

export function useLifeHooks(onMountedHandler: () => void, onDestroyedHandler: () => void = () => undefined) {
    useEffect(() => {
        onMountedHandler();
        return onDestroyedHandler;
    }, []);
}

export function useSignalEffects(handlers: Array<SignalEffectHandler>) {
    useEffect(() => {
        handlers.forEach((handler: SignalEffectHandler) => effect(handler));
    }, []);
}
