export async function dynamicLoadComponent(path: string = 'components/Undefined.tsx') {
    // Note: Vite has several limitations to import dynamically
    // Refer: https://github.com/rollup/plugins/tree/master/packages/dynamic-import-vars#limitations
    const partialPath = path.split('/');
    const fileName = (partialPath.pop() as string).replace('.tsx', '');
    const parentPath = partialPath.join('/');
    const module = await import(`../${parentPath}/${fileName}.tsx`);
    const Component = module.default as unknown as JSX.Element;
    // eslint-disable-next-line
    // @ts-ignore
    return <Component />;
}
