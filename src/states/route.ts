import SignalStore from '../expansions/signal-store.ts';
import { computed } from '@preact/signals-react';


const [ loadingCount, setLoadingCount ] = SignalStore.createState<number>(0);

const isFirstLoad = computed(() => loadingCount.value === 1);

const [ url, _setURL ] = SignalStore.createState<string>('');

function setURL(value: string) {
    _setURL(value);
    setLoadingCount(loadingCount.value + 1);
}

const [ hash, setHash ] = SignalStore.createState<string>('');

const [ navigateTarget, setNavigateTarget ] = SignalStore.createState<string|undefined>(undefined);

export default {
    isFirstLoad,
    url,
    hash,
    navigateTarget,
    setURL,
    setHash,
    setNavigateTarget,
};
