import { DocMenuItemMeta } from '../core/doc-menu-services.ts';
import SignalStore from '../expansions/signal-store.ts';


const [ metaItems, setMetaItems ] = SignalStore.createState<Array<DocMenuItemMeta>>([]);

const [ isScrollingManually, setIsScrollingManually ] = SignalStore.createNonState<boolean>(false);

export default {
    metaItems,
    isScrollingManually,
    setMetaItems,
    setIsScrollingManually,
}
