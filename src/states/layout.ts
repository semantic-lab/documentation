import { TreeItem } from '../core/side-menu-services.ts';
import SignalStore from '../expansions/signal-store.ts';

const [ sideMenuCollapsed, setSideMenuCollapsed ] = SignalStore.createState<boolean>(false);

const [ sideMenuList, setSideMenuList ] = SignalStore.createState<Array<TreeItem>>([]);

const [ openedDirs, setOpenedDirs ] = SignalStore.createState<Array<TreeItem>>([]);

const [ popupType, setPopupType ] = SignalStore.createState<string>('');

export default {
    sideMenuCollapsed,
    sideMenuList,
    openedDirs,
    popupType,
    setSideMenuCollapsed,
    setSideMenuList,
    setOpenedDirs,
    setPopupType,
};
