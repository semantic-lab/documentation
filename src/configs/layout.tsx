export default {
    projectName: 'Documentation',

    /**
     * Setup logo which in navigation-bar.
     * The is three kind of logo type can be set up: Element (JSX.Element), Image (path) and Text.
     * The logo size default as 240px * 48px, you can modify nac.scss if necessary.
     */
    logo: {
        element: null,
        image: null,
        text: 'Documentation',
    },
};
