import './assets/app.scss';
import './assets/components/content.scss';
import './assets/components/doc.scss';
import './assets/components/button.scss';
import './assets/theme/dark.scss';
import Nav from './components/Nav.tsx';
import SideMenu from './components/SideMenu.tsx';
import DocMenu from './components/DocMenu.tsx';
import Article from './components/article.tsx';
import DocMenuServices from './core/doc-menu-services.ts';
import DocStore from './states/doc.ts';
import FullScreenPopup from './components/FullScreenPopup.tsx';


function App() {
    const onScrolling = (e: { nativeEvent: Event }) => {
        const event = e.nativeEvent;
        DocMenuServices.changeActiveChapterByScrolling([
            event,
            DocStore.isScrollingManually.value,
            DocStore.metaItems.value,
            DocStore.setMetaItems,
        ]);
    }

    return (
        <div className="layout dark">
            <Nav />
            <div className="content">
                <SideMenu />
                <div className="doc" onScroll={onScrolling}>
                    <Article />
                    <DocMenu />
                </div>
                <FullScreenPopup />
            </div>
        </div>
    );
}

export default App;
