export default {
    deepCopy<T>(data: T) {
        return JSON.parse(JSON.stringify(data));
    },
    // eslint-disable-next-line
    isUnset(value: any, defineCases: Array<any> = [ null, undefined ]) {
        return defineCases.includes(value);
    },
    // eslint-disable-next-line
    deepValue(source: any, path: string) {
        let result = source;
        const keys = path.split('.');
        for (const key of keys) {
            try {
                // eslint-disable-next-line
                if (!result.hasOwnProperty(key)) return undefined;
                result = result[key];
            } catch {
                return undefined;
            }
        }
        return result;
    },
};
