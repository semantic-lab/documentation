import XObject from './x-object.ts';

// eslint-disable-next-line
export interface SwitchCase<T = (Promise<any>|any) | any> {
    condition: () => boolean;
    exec: () => T;
}

export interface SwitchOption {
    mode?: 'find' | 'filter';
    ignoreException?: boolean;
}

export default {
    // eslint-disable-next-line
    async switching(cases: Array<SwitchCase<Promise<any> | any>>, options: SwitchOption = { mode: 'find', ignoreException: true }) {
        const { mode, ignoreException } = options;
        const caseMeetCondition = ({ condition }: SwitchCase) => condition();
        const toExecutorResult = async ({ exec }: SwitchCase) => await exec();

        try {
            if (mode === 'find') {
                const meetCase = cases.find(caseMeetCondition);
                return await meetCase?.exec();
            }

            if (mode === 'filter') {
                const meetCases = cases.filter(caseMeetCondition);
                return await Promise.all(meetCases.map(toExecutorResult));
            }

        } catch (error) {
            if (ignoreException) return;
            throw error;
        }
    },

    // eslint-disable-next-line
    switchingSync(cases: Array<SwitchCase<any>>, options: SwitchOption = { mode: 'find', ignoreException: true }) {
        const { mode } = options;
        const ignoreException = XObject.isUnset(options?.ignoreException) ? true : options.ignoreException;
        const caseMeetCondition = ({ condition }: SwitchCase) => condition();
        const toExecutorResult = async ({ exec }: SwitchCase) => await exec();

        try {
            if (mode === 'find') {
                const meetCase = cases.find(caseMeetCondition);
                return meetCase?.exec();
            }

            if (mode === 'filter') {
                const meetCases = cases.filter(caseMeetCondition);
                return meetCases.map(toExecutorResult);
            }

        } catch (error) {
            if (ignoreException) return;
            throw error;
        }
    },
};
