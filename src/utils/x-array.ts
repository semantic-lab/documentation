export default {
    add<T>(target: T, to: Array<T>): Array<T> {
        const isExist = to.includes(target);
        if (isExist) return to;
        return [ ...to, target ];
    },

    merge<T>(...sources: Array<T>) {
        const sourceInStr = sources.map((item: T) => JSON.stringify(item));
        const sourceSet = new Set(sourceInStr);
        return [...sourceSet].map((itemStr: string) => JSON.parse(itemStr));
    },

    remove<T>(target: T, from: Array<T>) {
        const index = from.indexOf(target);
        const targetExist = index !== -1;
        if (!targetExist) return from;
        const copiedFrom = [...from];
        copiedFrom.splice(index, 1);
        return copiedFrom;
    },

    removeBy(conditions: Array<[ key: string, value: unknown ]>, from: Array<unknown>) {
        const index = from.findIndex((item: unknown) => conditions.every(([key, value]) => {
            try {
                // eslint-disable-next-line
                // @ts-ignore
                return item[key] === value;
            } catch {
                return false;
            }
        }));
        const targetExist = index !== -1;
        if (!targetExist) return from;
        const copiedFrom = [...from];
        copiedFrom.splice(index, 1);
        return copiedFrom;
    },

    // eslint-disable-next-line
    searchBy<T>(conditions: Record<string, any>) {
        // eslint-disable-next-line
        // @ts-ignore
        return (item: T) => Object.entries(conditions).every(([ key, value ]) => item[key] === value);
    },
};
