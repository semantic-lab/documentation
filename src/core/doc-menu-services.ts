import XArray from '../utils/x-array.ts';
import XConditions from '../utils/x-conditions.ts';
import XFunction from '../utils/x-function.ts';

export type DocMenuItemMeta = {
    index: number;
    element: Element;
    text: string;
    hash: string;
    classList: Array<string>;
};

export function getMenuItemMetas(url: string): Array<DocMenuItemMeta> {
    return url
        ? getHeaderElements().map(toMenuItem)
        : [];
}

type NavigateToItemProps = [
    metas: Array<DocMenuItemMeta>,
    targetIndex: number,
    addHashOnURL: (hash: string) => void,
    setIsScrollingManually: (isManually: boolean) => void,
    setMetaItems: (metas: Array<DocMenuItemMeta>) => void,
];

type ChangeActiveChapterByScrolling = [
    event: Event,
    isScrollingManually: boolean,
    metas: Array<DocMenuItemMeta>,
    setMetaItems: (value: Array<DocMenuItemMeta>) => void,
];

export async function navigateToItem([ metas, targetIndex, addHashOnURL, setIsScrollingManually, setMetaItems ]: NavigateToItemProps) {
    setIsScrollingManually(true);

    const targetMeta = metas[targetIndex];
    const updatedMetas = targetMeta
        ? modifyActiveItem(metas, targetIndex)
        : metas;
    if (targetMeta) {
        scrollHeaderInToView(targetIndex);
        addHashOnURL(targetMeta.hash);
    }

    await waitUntilNavigated(updatedMetas[targetIndex]);
    setIsScrollingManually(false);
    setMetaItems(updatedMetas);
}

export function findIndexByHash(hash: string, source: Array<DocMenuItemMeta>): number {
    return source.findIndex(XArray.searchBy({ hash }));
}

export function browserNavigateToChapter(hashList: [ string, string ], navigateFn: (to: string) => void) {
    const [ stateHash, currentURLHash ] = hashList;
    const alreadyChangeAndRedirect = stateHash === currentURLHash;
    if (alreadyChangeAndRedirect) return;

    const toRoot = stateHash === '';
    const destination = toRoot ? '#' : stateHash;
    navigateFn(destination);
}

export function changeActiveChapterByScrolling([ event, isScrollingManually, metas, setMetaItems ]: ChangeActiveChapterByScrolling) {
    XConditions.switchingSync([
        {
            condition: () => isScrollingManually,
            exec: XFunction.skip,
        },
        {
            condition: () => metas.length === 0,
            exec: XFunction.skip,
        },
        {
            condition: () => !event?.target,
            exec: XFunction.skip,
        },
        {
            condition: () => true,
            exec: () => {
                const scrollElement = event.target as Element;
                const latestMetaItems = detectedAndModifyActiveItem(metas, scrollElement);
                setMetaItems(latestMetaItems);
            }
        }
    ]);
}

function waitUntilNavigated(targetMeta: DocMenuItemMeta) {
    const scrollElement = document.querySelector('.doc');
    return new Promise(resolve => {
        const end = () => {
            clearInterval(timer);
            resolve(undefined);
        };
        const timer = setInterval(() => {
            XConditions.switchingSync([
                {
                    condition: () => !scrollElement,
                    exec: end,
                },
                {
                    condition: () => !targetMeta,
                    exec: end,
                },
                {
                    condition: () => {
                        const { scrollTop, scrollHeight, clientHeight } = scrollElement as Element;
                        const isScrollAtEnd = (scrollHeight - scrollTop - clientHeight <= 0);
                        return isScrollAtEnd;
                    },
                    exec: end,
                },
                {
                    condition: () => !(isInTheView(targetMeta)),
                    exec: XFunction.skip,
                },
                {
                    condition: () => true,
                    exec: end,
                }
            ]);
        }, 1000 * 0.05);
    });
}

function scrollHeaderInToView(index: number) {
    const targetElement = getHeaderElements()[index];
    targetElement.scrollIntoView({ behavior: 'smooth' })
    XConditions.switchingSync([
        {
            condition: () => !targetElement,
            exec: () => XFunction.skip(),
        },
        {
            condition: () => true,
            exec: () => targetElement.scrollIntoView({ behavior: 'smooth' }),
        }
    ]);
}

function modifyActiveItem(metas: Array<DocMenuItemMeta>, activeItemIndex: number): Array<DocMenuItemMeta> {
    const activeClassName = 'active';
    const originActiveItem = metas.find(({ classList }) => classList.includes(activeClassName));
    if (originActiveItem) {
        originActiveItem.classList = XArray.remove(activeClassName, originActiveItem.classList);
    }
    metas[activeItemIndex].classList.push(activeClassName);
    return [ ...metas ];
}

function getHeaderElements() {
    const headerTags = 'h1, h2, h3, h4, h5, h6';
    const articleElement = document.querySelector('.article');
    return articleElement === null
        ? []
        : [...articleElement.querySelectorAll(headerTags)];
}

function toMenuItem(element: Element, index: number): DocMenuItemMeta {
    const htmlElement = element as HTMLElement
    const headerClassName = htmlElement.tagName.replace('H', 'header-');
    const text = htmlElement.innerText;
    const hash = index === 0 ? '' : `#${ encodeURI(text.toLowerCase().replace(/\s/g, '-')) }`;
    return {
        index,
        element,
        text,
        hash,
        classList: [ 'item', headerClassName],
    };
}

function isInTheView({ element }: DocMenuItemMeta) {
    const navElementHeight = 40;
    const threshold = 0;
    const marginTop = Number(window.getComputedStyle(element).marginTop.replace('px', ''));
    return (element.getBoundingClientRect().top - marginTop - navElementHeight) <= threshold;
}

function detectedAndModifyActiveItem(metas: Array<DocMenuItemMeta>, scrollElement: Element) {
    const { scrollTop } = scrollElement;
    if (scrollTop === 0) return modifyActiveItem(metas, 0);

    const candidateMetas = metas
        .filter(isInTheView)
        .reverse();
    const index = candidateMetas.length === 0 ? 0 : candidateMetas[0].index;
    return modifyActiveItem(metas, index);
}

export default {
    getMenuItemMetas,
    navigateToItem,
    findIndexByHash,
    browserNavigateToChapter,
    changeActiveChapterByScrolling,
}
