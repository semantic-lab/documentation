type SearchByPressEnterArgs = [
    event: KeyboardEvent,
    keywords: string,
    // eslint-disable-next-line
    setSearchCriteria: (value: Record<string, any> | undefined) => void,
    setPopupType: (value: string) => void,
];

type SearchByClickOnTagArgs = [
    tag: string,
    // eslint-disable-next-line
    setSearchCriteria: (value: Record<string, any> | undefined) => void,
    setPopupType: (value: string) => void,
];

type SearchArgs = [
    // eslint-disable-next-line
    searchCriteria: Record<string, any> | undefined,
    // eslint-disable-next-line
    setSearchCriteria: (value: Record<string, any> | undefined) => void,
    setPopupType: (value: string) => void,
];

function searchByPressEnter([ event, keywords, setSearchCriteria, setPopupType ]: SearchByPressEnterArgs) {
    const isPressEnter = event.key === 'Enter';
    if (!isPressEnter) return;
    const isSearching = !!keywords;
    const searchCriteria = isSearching
        ? { '*': keywords }
        : undefined;
    search([ searchCriteria, setSearchCriteria, setPopupType ]);
}

function searchByClickOnTag([ tag, setSearchCriteria, setPopupType ]: SearchByClickOnTagArgs) {
    const isSearching = !!tag;
    const searchCriteria = isSearching
        ? { 'meta.tags': tag }
        : undefined;
    search([ searchCriteria, setSearchCriteria, setPopupType ]);
}

function search([ searchCriteria, setSearchCriteria, setPopupType ]: SearchArgs) {
    const isSearching = searchCriteria && Object.keys(searchCriteria).length > 0;
    setSearchCriteria(searchCriteria);
    setPopupType(isSearching ? 'search' : '');
}

export default {
    searchByPressEnter,
    searchByClickOnTag,
    search,
};
