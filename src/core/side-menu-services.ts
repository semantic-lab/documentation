import XObject from '../utils/x-object.ts';
import XArray from '../utils/x-array.ts';
import XFunction from '../utils/x-function.ts';
import XConditions from '../utils/x-conditions.ts';

export type RouteItem = {
    name: string;
    path: string;
    href: string;
    isDir: boolean;
    level: number;
    children?: Array<RouteItem>;
    // eslint-disable-next-line
    meta?: Record<string, any>;
};

export type TreeItem = {
    name: string;
    href: string; // full path
    isDir: boolean;
    level: number;
    classList: Array<string>;
    // eslint-disable-next-line
    meta?: Record<string, any>;
};

export type ClickOnSideMenuComponentArgs = [
    targetItem: TreeItem,
    sideMenuList: Array<TreeItem>,
    openItems: Array<TreeItem>,
    modifyOpenItems: (openItems: Array<TreeItem>) => void,
    navigateTargetMutator: (href: string|undefined) => void,
];

const OPENED_CLASS_NAME = 'opened';

const ACTIVE_CLASS_NAME = 'active';

const ITEM_CLASS_NAME = 'item';

const SIDE_MENU_CLASS_NAME = 'side-menu';

const COLLAPSE_CLASS_NAME = 'collapse';

export function toTreeList(routes: Array<RouteItem>): Array<TreeItem> {
    return toTreeListRecursive(routes);
}

export function getTreeListForView(treeList: Array<TreeItem>, openItems: Array<TreeItem>): Array<TreeItem> {
    const shownItems = treeList.filter(isShownItem(openItems));
    return XObject.deepCopy(shownItems);
}
export function getOpenDirsByHref(source: Array<TreeItem>, href: string, currentOpenItems: Array<TreeItem> = []): Array<TreeItem> {
    const targetItem = source.find(XArray.searchBy({ href }));
    return XConditions.switchingSync([
        {
            condition: () => !targetItem,
            exec: () => [],
        },
        {
            condition: () => !!targetItem,
            exec: () => {
                const result = source.filter(isSelfOrParentDirOf(targetItem as TreeItem));
                result.forEach(addOpenedClassName);
                const openItemsOfHref = XObject.deepCopy(result) as Array<TreeItem>;
                return XArray.merge(...currentOpenItems, ...openItemsOfHref);
            },
        }
    ]) as Array<TreeItem>;
}

export function navigateToDocument(navigateTarget: string|undefined, navigateFn: (to: string) => void) {
    return navigateTarget
        ? navigateFn(navigateTarget)
        : XFunction.skip();
}

export function generateShownSideMenu(url: string, source: Array<TreeItem>, openItems: Array<TreeItem>) {
    return XConditions.switchingSync([
        {
            condition: () => !url,
            exec: () => [],
        },
        {
            condition: () => !!url,
            exec: () => {
                const result = getTreeListForView(source, openItems);
                const activeItem = result.find(XArray.searchBy({ href: url }));
                activeItem && addClassNameTo(activeItem, ACTIVE_CLASS_NAME);
                return result;
            },
        }
    ]);
}

export function computedSideMenuItemComponentInfo(target: TreeItem): [ string, boolean, boolean, boolean ] {
    const className = [ ...target.classList, ITEM_CLASS_NAME ].join(' ');
    const isCollapsedDir = target.isDir && (!target.classList.includes(OPENED_CLASS_NAME));
    const isOpenedDir = target.isDir && target.classList.includes(OPENED_CLASS_NAME);
    const isFile = !target.isDir;
    return [ className, isCollapsedDir, isOpenedDir, isFile ];
}

export function clickOnSideMenuComponent([ targetItem, sideMenuList, openItems, modifyOpenItems, navigateTargetMutator ]: ClickOnSideMenuComponentArgs) {
    XConditions.switchingSync([
        {
            condition: () => targetItem.isDir,
            exec: () => {
                const toCollapse = targetItem.classList.includes(OPENED_CLASS_NAME);
                const latestOpenItems = toCollapse
                    ? collapseTreeNode(sideMenuList, targetItem, openItems)
                    : openTreeNode(sideMenuList, targetItem, openItems);
                modifyOpenItems(latestOpenItems)
            }
        },
        {
            condition: () => !targetItem.isDir,
            exec: () => {
                navigateTargetMutator(`${targetItem.href}`);
            }
        }
    ]);
}

export function computedSideMenuContainerComponent(sideMenuCollapsed: boolean) {
    const classList = sideMenuCollapsed
        ? [ SIDE_MENU_CLASS_NAME, COLLAPSE_CLASS_NAME ]
        : [ SIDE_MENU_CLASS_NAME ];
    return classList.join(' ');
}

// eslint-disable-next-line
export function filterBySearchCriteria(source: Array<TreeItem>, searchCriteria: Record<string, any>|undefined) {
    if (!searchCriteria) return [];
    return source.filter(matchPredicate(searchCriteria, 'filter'));
}

// eslint-disable-next-line
export function findBySearchCriteria(source: Array<TreeItem>, searchCriteria: Record<string, any>|undefined) {
    if (!searchCriteria) return undefined;
    return source.find(matchPredicate(searchCriteria, 'find'));
}

function toTreeListRecursive(routes: Array<RouteItem>): Array<TreeItem> {
    const list: Array<TreeItem> = [];

    routes.forEach((route: RouteItem) => {
        const treeItem = toTreeItem(route);
        list.push(treeItem);
        if (!route.isDir || !route.children) return;

        const childrenTreeItems = toTreeListRecursive(route.children);
        list.push(...childrenTreeItems);
    });

    return list;
}

function toTreeItem(route: RouteItem): TreeItem {
    return {
        name: route.name,
        href: route.href,
        isDir: route.isDir,
        level: route.level,
        classList: [ `level-${ route.level }` ],
        meta: route.meta,
    };
}

function isShownItem(openItems: Array<TreeItem>) {
    return (treeItem: TreeItem) => {
        const isFirstLevel = treeItem.level === 0;
        if (isFirstLevel) return true;

        const matchedOpenItem = openItems.find((openItem: TreeItem) => {
            const isEqualsOrSubItem = (new RegExp(`^${openItem.href}`, 'g')).test(treeItem.href);
            const isEqualsOrNextLevel = openItem.level === treeItem.level || openItem.level === treeItem.level - 1;
            return isEqualsOrSubItem && isEqualsOrNextLevel;
        });
        return !!matchedOpenItem;
    }
}

function openTreeNode(treeList: Array<TreeItem>, targetItem: TreeItem, openItems: Array<TreeItem>): Array<TreeItem> {
    if (!targetItem.isDir) return openItems;
    const originItem = treeList.find(({ href }) => href === targetItem.href);
    if (!originItem) return openItems;
    addOpenedClassName(originItem);
    return [...openItems, originItem];
}

function collapseTreeNode(treeList: Array<TreeItem>, targetItem: TreeItem, openItems: Array<TreeItem>): Array<TreeItem> {
    let result = openItems;
    openItems
        .filter(({ href, level, isDir}) => isDir && href.includes(targetItem.href) && level >= targetItem.level)
        .forEach(({ href }) => {
            const originItem = treeList.find((treeItem: TreeItem) => treeItem.href === href);
            originItem && (originItem.classList = XArray.remove('opened', originItem.classList));
            result = XArray.removeBy([[ 'href', href ]], result) as Array<TreeItem>;
        });
    return result;
}

function isSelfOrParentDirOf(targetItem: TreeItem) {
    return ({ isDir, href, level }: TreeItem) => isDir && targetItem.href.includes(href) && level <= targetItem.level;
}

function addOpenedClassName(target: TreeItem) {
    target.classList = XArray.add(OPENED_CLASS_NAME, target.classList);
}

function addClassNameTo(target: TreeItem, className: string) {
    target.classList = XArray.add(className, target.classList);
}

// eslint-disable-next-line
function matchPredicate(searchCriteria: Record<string, any>, mode: 'find' | 'filter'): (data: TreeItem) => boolean {
    // eslint-disable-next-line
    function compare(source: any, target: string) {
        const lowerStr = (data: never) => JSON.stringify(data).replace(/^"/g, '').replace(/"$/g, '').toLowerCase();
        const sourceInLowerCase = lowerStr(source as never);
        const targetInLowerCase = lowerStr(target as never);
        if (mode === 'find') return sourceInLowerCase === targetInLowerCase;
        if (mode === 'filter') return sourceInLowerCase.includes(targetInLowerCase);
        return false;
    }

    return (data: TreeItem) => {
        return Object
            .entries(searchCriteria)
            .some(([ searchingKey, searchingValue ]) => {
                return XConditions.switchingSync([
                    {
                        condition: () => !searchingKey,
                        exec: () => false,
                    },
                    {
                        condition: () => searchingKey === '*',
                        exec: () => !!(Object
                            .values(data)
                            .find((value) => compare(value, searchingValue))),
                    },
                    {
                        condition: () => searchingKey.includes('.'),
                        exec: () => compare(XObject.deepValue(data, searchingKey), searchingValue),
                    },
                    {
                        condition: () => !!searchingKey,
                        exec: () => {
                            // eslint-disable-next-line
                            // @ts-ignore
                            return compare(data[searchingKey], searchingValue);
                        },
                    },
                ]);
            });
    }
}

export default {
    toTreeList,
    getOpenDirsByHref,
    navigateToDocument,
    generateShownSideMenu,
    computedSideMenuItemComponentInfo,
    clickOnSideMenuComponent,
    computedSideMenuContainerComponent,
    filterBySearchCriteria,
    findBySearchCriteria,
};
