import fs from 'fs';
import { readSync } from 'to-vfile';
import { matter } from 'vfile-matter';
import chalk from 'chalk';

// region ACTIONS
async function scanDocsFolder() {
    const [ routes, modules ] = await scanDocsRecursive('./src/docs', '', 0);
    return { routes, modules };
}

function loadRoutesConfigTemplate() {
    const path = './scripts/templates/routes.tsx.template';
    const options = { encoding: 'utf-8', flag: 'r+' };
    return fs.readFileSync(path, options);
}

function initRoutesConfigContent(template, routes, modules) {
    const routesStr = JSON.stringify(routes, null, 4)
        .replace(/"</g, '<')
        .replace(/>"/g, ">");

    return template
        .replace('// MODULES', modules.join(''))
        .replace('[ /* ROUTES */ ]', routesStr);
}

function modifyRoutesConfig(content) {
    const path = './src/configs/routes.tsx';
    const options = { encoding: 'utf-8', flag: 'w' };
    fs.writeFileSync(path, content, options);
}
// endregion

// region UTILS
async function scanDocsRecursive(fsPath, link, level) {
    const routes = [];
    const modules = [];

    for (let name of fs.readdirSync(fsPath)) {
        const itemName = toMenuItemName(name);
        const itemPath = `${fsPath}/${name}`;
        const isDir = fs.lstatSync(itemPath).isDirectory();
        const path = toURLPath(name);
        const href = `${link}/${path}`;

        if (isDir) {
            const nextLevel = level + 1;
            const [children, subModules] = await scanDocsRecursive(itemPath, href, nextLevel);
            const route = { name: itemName, path, href, isDir, level, children };
            routes.push(route);
            modules.push(...subModules);
        }

        if (!isDir && isMDX(name)) {
            const tagName = toJSXElementTagName(name);
            const modulePath = itemPath.replace('./src/docs', '../docs');
            const element = `<${tagName} />`;
            const meta = extractMeta(itemPath);
            const route = { name: itemName, path, href, isDir, level, element, meta };
            const module = toImportSyntax(tagName, modulePath);
            routes.push(route);
            modules.push(module);
        }

    }

    return [ routes, modules ];
}

function isMDX(name) {
    return /.*\.mdx$/g.test(name);
}

function toMenuItemName(name) {
    return name.replace('.mdx', '');
}

function toURLPath(name) {
    return name.replace('.mdx', '').replace(/\s/g, '-').toLowerCase();
}

function toJSXElementTagName(name) {
    const chars = [...(
        name
            .replace('.mdx', '')
            .replace(/\s/g, '')
            .replace(/-/g, '_')
    )];
    chars[0] = chars[0].toUpperCase();
    return `Doc${chars.join('')}`;
}

function extractMeta(path) {
    try {
        const doc = readSync(path, { encoding: 'utf-8' });
        if (!doc) return undefined;
        matter(doc);
        return Object.keys(doc.data.matter).length > 0 ? doc.data.matter : undefined;
    } catch(e) {
        return undefined;
    }
}

function toImportSyntax(tagName, modulePath) {
    return `import ${ tagName } from '${modulePath}';\n`;
}
// endregion

// region MAIN
(async () => {
    const { routes, modules } = await scanDocsFolder();
    const template = loadRoutesConfigTemplate();
    const routesConfig = initRoutesConfigContent(template, routes, modules);
    modifyRoutesConfig(routesConfig);

    const message = chalk.hex('#e0f495').bold('Updating Routes Successfully!');
    console.log(message);
})();
// endregion
